package com.shuyangyang.component;

import cn.hutool.core.util.StrUtil;
import com.shuyangyang.util.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * 工具控制台面板
 *
 * @author Yang Yang Shu
 * @version 1.0
 */
public class ControlComponent extends Component {

    private final JFrame jFrame = new JFrame("身份证号银行卡号港澳通行证号生成器");
    private final Container c = jFrame.getContentPane();
    private final JTextArea jTextArea = new JTextArea();
    private final JScrollPane jScrollPane = new JScrollPane(jTextArea);
    private final Font font = new Font("宋体", Font.PLAIN, 16);
    private final Font titleFont = new Font("宋体", Font.BOLD, 16);
    private final Font smallFont = new Font("宋体", Font.PLAIN, 12);

    public ControlComponent() {
        //设置窗体的位置及大小
        jFrame.setBounds(600, 200, 910, 700);

        // 设置菜单
        JMenuBar jMenuBar = new JMenuBar();
        JMenu jMenu = new JMenu("皮肤");
        jMenu.setFont(font);

        // 绑定菜单动作
        new ComponentAction().bindMenuAction(jMenu, font);

        JMenu aboutMenu = new JMenu("关于软件");
        aboutMenu.setFont(font);
        JMenuItem aboutMenuItem = new JMenuItem("About");
        aboutMenuItem.setFont(font);
        aboutMenuItem.addActionListener(new AbstractAction() {
            private static final long serialVersionUID = 5493490416881069681L;

            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(ControlComponent.this, "仅供系统开发人员测试使用，所有信息均为虚构,请不要用于任何非法用途,且自行承担后果和责任。", "关于软件", JOptionPane.WARNING_MESSAGE);
            }
        });

        aboutMenu.add(aboutMenuItem);

        JMenuItem authorMenuItem = new JMenuItem("作者");
        authorMenuItem.setFont(font);
        authorMenuItem.addActionListener(new AbstractAction() {

            private static final long serialVersionUID = -4473895512351094556L;

            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(ControlComponent.this, "©束洋洋 喜欢请给作者的 Gitee点一个Star，谢谢！", "关于作者", JOptionPane.INFORMATION_MESSAGE);
            }
        });


        aboutMenu.add(authorMenuItem);

        jMenuBar.add(jMenu);
        jMenuBar.add(aboutMenu);
        jFrame.setJMenuBar(jMenuBar);

        //布局管理器
        c.setLayout(new BorderLayout());
        //设置按下右上角X号后关闭
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //初始化窗体控件
        init();
        //设置窗体可见
        jFrame.setVisible(true);
    }

    /**
     * 初始化主面板
     */
    void init() {
        // 输入框
        JPanel jPanel = new JPanel();
        jPanel.setLayout(null);
        // Banner
        ComponentUtil.createTextLabel(jPanel, "仅供系统开发人员测试使用，所有信息均为虚构,请不要用于任何非法用途,且自行承担后果和责任。", titleFont, 60, 0, 850, 100);
        ComponentUtil.createTextLabel(jPanel,
                "免责声明：身份证号大全和姓名由程序随机组合而成，所有信息均为虚构生成，不会泄密真实公民隐私信息，也非现实生活中真实的身份证号码和真实姓名；", smallFont, 30, 30, 850, 100);
        ComponentUtil.createTextLabel(jPanel, "身份证号码和姓名仅供测试或用在必须输入身份证号码和姓名的网站上，请不要将身份证号码和姓名用于任何非法用途且自行承担使用本工具的任何后果和责任。", smallFont, 30, 50, 860, 100);
        // 多行文本区域
        jTextArea.setLineWrap(true);
        jTextArea.setFont(font);

        // 底部按钮
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout());

        JButton generationBtn = ComponentUtil.createButton(font, "生成");
        JButton resetBtn = ComponentUtil.createButton(font, "重置");
        doButton(generationBtn, resetBtn);

        jScrollPane.setBounds(30, 120, 850, 490);
        jPanel.add(jScrollPane);
        c.add(jPanel, "Center");
        buttonPanel.add(generationBtn);
        buttonPanel.add(resetBtn);
        c.add(buttonPanel, "South");
    }

    /**
     * 按钮动作事件
     *
     * @param generationBtn 生成按钮
     * @param resetBtn      重置按钮
     */
    private void doButton(JButton generationBtn, JButton resetBtn) {
        generationBtn.addActionListener(e -> {
            String result = StrUtil.format("姓名: {} \n手机号: {}\n身份证: {}\n银行卡: {}\n港澳台通行证：{}\n\n",
                    FullNameUtil.getFullName(), PhoneNumberUtil.getPhoneNumber(), IdNumberUtil.getIdNumber(), BankPinUtil.getBankPin(), CorrespondenceUtil.getCorrespondence());
            jTextArea.append(result);
        });

        resetBtn.addActionListener(e -> {
            jTextArea.setText("");
        });
    }
}
