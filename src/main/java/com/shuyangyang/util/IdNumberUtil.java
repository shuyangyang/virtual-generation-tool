package com.shuyangyang.util;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;

import java.time.LocalDate;

/**
 * 身份证号码生成工具类
 *
 * @author Yang Yang Shu
 */
public class IdNumberUtil {

    private IdNumberUtil() {
    }

    /**
     * 前1、2位数字:省、自治区、直辖市代码
     */
    private static final String[] PROVINCES_CODE = {
            "11", "12", "13", "14", "15", "21", "22", "23",
            "31", "32", "33", "34", "35", "36", "37", "41",
            "42", "43", "44", "45", "46", "50", "51", "52",
            "53", "54", "61", "62", "63", "64", "65", "71",
            "81", "82"
    };

    /**
     * 第3、4位数字: 地级市、盟、自治州代码
     */
    private static final String[] CITY_CODE = {
            "01", "02", "03", "04", "05", "06", "07", "08",
            "09", "10", "21", "22", "23", "24", "25", "26",
            "27", "28"
    };

    /**
     * 第5、6位数字:县、县级市、区代码
     */
    private static final String[] COUNTY_CODE = {
            "01", "02", "03", "04", "05", "06", "07", "08",
            "09", "10", "21", "22", "23", "24", "25", "26",
            "27", "28", "29", "30", "31", "32", "33", "34",
            "35", "36", "37", "38"
    };

    /**
     * 第18位数字: 校验码
     */
    private static final String[] CHECK_CODE = {"1", "0", "X", "9", "8", "7", "6", "5", "4", "3", "2"};


    /**
     * 随机生成身份证号码
     *
     * @return 身份证号码
     */
    public static String getIdNumber() {
        // 前1、2位数字表示：所在省份的代码
        String province = PROVINCES_CODE[RandomUtil.randomInt(0, PROVINCES_CODE.length - 1)];
        // 第3、4位数字表示：所在城市的代码
        String city = CITY_CODE[RandomUtil.randomInt(0, CITY_CODE.length - 1)];
        // 第5、6位数字表示：所在区县的代码
        String county = COUNTY_CODE[RandomUtil.randomInt(0, COUNTY_CODE.length - 1)];
        // 第7-14位数字表示：出生年、月、日
        String birthDate = getBirthDate();
        // 第15、16位数字表示：所在地的派出所的代码; 第17位数字表示性别：奇数表示男性，偶数表示女性
        String noCode = String.valueOf(RandomUtil.randomInt(100, 999));
        // 第18位数字是校检码
        String checkCode = CHECK_CODE[RandomUtil.randomInt(0, CHECK_CODE.length - 1)];
        return province + city + county + birthDate + noCode + checkCode;
    }


    /**
     * 随机生成1950-2020年之间的随机出生年月
     *
     * @return 出生年月日数字字符串
     */
    public static String getBirthDate() {
        int minDay = (int) LocalDate.of(DateUtil.thisYear() - 120, 1, 1).toEpochDay();
        int maxDay = (int) LocalDate.of(DateUtil.thisYear(), 8, 1).toEpochDay();
        long randomDay = minDay + RandomUtil.randomInt(0, maxDay - minDay);
        LocalDate localDate = LocalDate.ofEpochDay(randomDay);
        return StrUtil.replace(localDate.toString(), "-", "");
    }
}
