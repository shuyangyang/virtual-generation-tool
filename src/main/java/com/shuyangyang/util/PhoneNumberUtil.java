package com.shuyangyang.util;

import cn.hutool.core.util.RandomUtil;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * 手机号码生成工具
 *
 * @author Yang Yang Shu
 */
public class PhoneNumberUtil {

    private PhoneNumberUtil() {
    }

    /**
     * 手机号码号段
     */
    private static final String[] PHONE_NO_SECTION = {
            "130", "131", "132", "133", "135", "136", "137", "138", "139",
            "150", "151", "157", "158", "170", "187", "189", "159", "178",
            "182", "183", "184", "153"
    };

    /**
     * 随机生成手机号码
     *
     * @return 手机号码
     */
    public static String getPhoneNumber() {
        int prefixNumber = RandomUtil.randomInt(0, PHONE_NO_SECTION.length - 1);
        String phoneSection = PHONE_NO_SECTION[prefixNumber];
        return IntStream.range(0, 8).mapToObj(i -> String.valueOf(RandomUtil.randomInt(0, 9))).collect(Collectors.joining("", phoneSection, ""));
    }
}
