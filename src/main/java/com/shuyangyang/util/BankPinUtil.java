package com.shuyangyang.util;

import cn.hutool.core.util.RandomUtil;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * 银行卡号生成工具类
 *
 * @author Yang Yang Shu
 */
public class BankPinUtil {

    private BankPinUtil() {
    }

    /**
     * 银行卡号前缀
     */
    private static final String[] PIN_PREFIX = {
            "622202", "622848", "622700", "622262", "621661", "622666", "622622", "622323",
            "622556", "622588", "622155", "622689", "622630", "622908", "621717", "622309"
    };


    /**
     * 随机生成银行卡号
     *
     * @return 银行卡号
     */
    public static String getBankPin() {
        return IntStream.range(0, 13).mapToObj(i -> String.valueOf(RandomUtil.randomInt(0, 9))).collect(Collectors.joining("", PIN_PREFIX[RandomUtil.randomInt(0, PIN_PREFIX.length - 1)], ""));
    }
}
