package com.shuyangyang.util;

import cn.hutool.core.util.RandomUtil;

/**
 * 港澳台通信证生成工具类
 * <p>
 * 由一个大写英文字母+8位数字组成
 * </p>
 *
 * @author Yang Yang Shu
 */
public class CorrespondenceUtil {
    private CorrespondenceUtil() {
    }

    /**
     * 随机生成港澳台通行号
     *
     * @return 港澳台通信行号
     */
    public static String getCorrespondence() {
        char[] letters = new char[26];
        char letter = 'A';
        for (int i = 0; i < 26; i++) {
            letters[i] = letter;
            letter++;
        }

        char correspondencePrefix = letters[RandomUtil.randomInt(0, letters.length - 1)];
        return correspondencePrefix + String.valueOf(RandomUtil.randomInt(0, 99999999));
    }

    public static void main(String[] args) {
        System.out.println(getCorrespondence());
    }
}
